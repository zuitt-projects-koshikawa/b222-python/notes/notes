# [Section] Input
# input() is similar to prompt() in JS that seeks to gather data from user input. Returns string data type.
# \n is a new line character (escape character)
username = input("What is your name: \n ")
print(f"Hello {username}! Welcome to Python Course.")

num1 = int(input("Please enter the first number: \n"))
num2 = int(input("Please enter the second number: \n"))
print(f"The sum of {num1} and {num2} is {int(num1) + int(num2)}")

# [Section] If-Else Statements
test_num = 75
# In python, the condition that the if statement has to assess is not enclosed in parenthesis. At the end of the statement, there is a colon, denoting that indented statements below will be executed

# the rule for indention is also applicable for other code syntax such as functions

if test_num >= 60:
    print("You passed!")
else:
    print("You failed!")

# if-else chains
# elif is the same as else if keyword in JS
test_num2 = int(input("Please enter a number for testing: \n"))

if test_num2 >= 0:
    print("The number is postive")
elif test_num2 == 0: 
    print("The number is zero")
else:
    print("The number is negative")

"""

Mini-activity
 - Create an if-else statement that determins if a number is divisible by 3, 5 or both
 - if it is divisible by 3, print "The number is divisible by 3"
 - if it is divisible by 5, print "The number is divisible by 5"
- if it is divisible by 3 and 5, print "The number is divisible by 3 and 5"
- if it is not divisible by 3 or 5, print "The number is not divisible by 3 or 5"

"""

check_num = int(input("Please enter a number to test: \n"))

if check_num % 3 == 0 and check_num % 5 == 0:
    print(f"The {check_num} is divisible by 3 and 5")
elif check_num % 3 == 0:
    print(f"The {check_num} is divisible by 3")
elif check_num % 5 == 0:
    print(f"The {check_num} is divisible by 5")
else:
    print(f"The {check_num} is not divisible by 3 or 5")

# [Section] For Loops
# While Loop
# performs a code block as long as the condition is true

i=1
while i <= 5:
    print(f"Current Value: {i}")
    i += 1

# [Section] For Loop
# Used to iterate over a sequence 

fruits = ["apple", "banana", "cherry"]

for indiv_fruit in fruits:
    print(indiv_fruit)

# range() method returns the sequence of the given number
# Range  method would allow us to iterate the values
"""
Syntax: 
ramge(stop)
ramge(start, stop)
range(start, stop, step)
"""
for x in range(6):
    print(f"Current Value: {x}")

for x in range(1, 10, 2):
    print(f"Current Value: {x}")

# [Section] Break and Continue Statements
# Break statement
# break statement is used to exit the loop
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1

# Continue statement
# Continue statement is used to skip the current iteration of the loop
# takes the control back to the top of the iteration
k = 1
while k < 6:
    k += 1 # signifies the top of the loop should we use continue statement
    if k == 3:
        continue
    print(k)

"""
The code below is not an infinite loop, but rather the continue statement is trying to find the "top" of the loop, which is the k += 1 statement that is skipped after the if statement.
k = -1
while k < 6:
    if k == 3:
        continue
    print(k)
    k += 1 
"""

