# import math

# ctrl + / - comments in python:
# instead of double /, python uses a pound sign (#): 1 liner

"""
	Although we have no kindbind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""

# Print is similar to console.log in node/js since it lets us to print messages in the console/terminal.
print("Hello World")

# [Section] Variables
# We don't have to use keywords in python to set our variables.
# Snake case is the convetion in terms of naming the variables in python. It uses underscores to in between words and uses.

age=35
middle_initial="C"
print(age)
print(middle_initial)
# delcaring and assigning values to multiple variables simultaneously is possible in python.
name1, name2, name3,name4 = "John", "Mary", "Bob", "Micheal"
print(name1)
print(name2)
print(name3)
print(name4)

# [Section] Data Types
# string
full_name = "John Doe"
secret_code = "Pa$$w0rd"
# number
num_of_days= 365 # integer
pi_approx = 3.1416 # float
complex_num = 1+5j # complex
print(complex_num)
# boolean
is_learning = True # capital T is important
is_learning = False # capital F is important

print("Hi! My name is " + full_name + " and I am " + str(age) + " years old.")
print(f"Hi! My name is {full_name} and I am {age} years old.") # f-string is a new way of formatting strings in python 3.6 and above like tempalte literals for JS.

# [Section] Type Casting
# is python's way of converting one data type into another since it does not have any type of coercion like in JS.
# print("My age is" + age) # this will throw an error since we are trying to concatenate a string and an integer.

# from in to string
print("My age is " + str(age)) # this will work since we are converting the integer into a string.
# from string to int
print(age + int("9876")) # this will work since we are converting the string into an integer.
# integer is different from float in python
print(age + int(98.87))

# [Section] Operators
print(2 + 10)  # addition
print(2 - 10)  # subtraction
print(2 * 10)  # multiplication
print(2 / 10)  # division
print(2 % 10)  # modulus
print(2 ** 10) #exponent
# print(math.sqrt(25)) # need to import from math library
# [Section] Assignment Operators
num1 = 3
print(num1)
num1 = num1 + 4
print(num1)

# [Section] Comparison Operators
# returns a boolean value
print (2 == 2) # equal to true
print (2 == "2") # equal to false since 2 is an integer and "2" is a string this is comparable to strict equality for JS

print (2 <= 2)
print (2 != 2)

# [Section] Logical Operators
# returns a boolean value

print(True and False) # and	
print(True or False) # or	
print(not False) # not
