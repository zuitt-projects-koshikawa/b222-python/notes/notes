# [Section] Lists
# list in Python is similar to the array in JS. They both provide a collection of data.

names = ["John", "Jane", "Jack", "Jill"]
programs = ['developer career', 'pi-shape', 'short courses']
durations = [260 , 180, 20]
truth_values = [True, False ,True, True, False]


print(names)

# list with different data types
sample_list = ["Apple", 3, False, "Potato", 4 ,True]
# allowed in python, just keep in mind the convention of having the same data type in each of the list
print(sample_list)

# getting the size of the list
print(len(programs))
# print(len("apple")) it can also be used in the string

# accessing the values inside the list
print(durations[1])
print(durations[0])

# since python has negative indeces, we can commonly use it to check the last element of the list
print(durations[-1]) # last element
print(names[-3]) # third element from the end of the list

# accessing non-existing indeces

# print(truth_values[20]) would return an error: index out of range

# Range of indeces
print(names[0:2]) # from index 0, print the elements unitl before index 2


"""
[Section] Mini-activity
1.) Create a list of names of 5 students
2.) Create a list of grades for the 5 students
3.) Using a loop, iterate through the list and print "The grade of <student> is <grade>."

"""

student_names = ["Jack", "And", "Jill", "Went", "Up"]
student_grades = [95, 88, 73, 65, 80]
i= 0
for x in range(len(student_names)):
    print("The grade of " + student_names[x] + " is " + str(student_grades[x]) + ".")

# or 

# while i < len(student_names):
#     print("The grade of " + student_names[i] + " is " + str(student_grades[i]) + ".")
#     i += 1

# Updating list
# Print the current value
print(f"Current Value: {names[2]}")

# Update the value
names[2] = 'Michael'
print(f"New value: {names[2]}")

# List Manipulation
# List methods that can be used to manipulate the elements within

# Adding List items -  the append() method allows inserting items to the end of the list

names.append('Bobby')
print(names)

# Deleting List Items - the "del" keyword can be used to delete item from a list
durations.append(360)
print(durations)

# Delete the last item from durations
del durations[-1]
print(durations)

# Memberhip checks - the "in" keyword checks if a given elements is in the list and returns true or false
print(20 in durations)
print(500 in durations)

# Sorting Lists - the sort() method sorts the list alphanumeriticallly, in ascending order by default
names.sort()
print(names)

# Looping through list
count = 0
while count < len(names):
    print(names[count])
    count += 1

# Dictionaries 
# Dictionaries are used to store data in key:value pairs, similar to objects in JS. Dictionaries are collections which are ordered, changeable, and do not allow duplicates.

# Ordered means that items have a defined order that cannot be changed
# Changeable means that we can change, add, or remove items after the dictionary has been created

person1 = {
    "name": "Brandon",
    "age": 28,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]

}

# To get the number of key:value pairs in a dicitonary, the len() function can be used again
print(len(person1))

# Access Values in dictionaries
# To get a value of an item in a dictionary, the key name can be used with a pair of square brackets
print(person1['name'])

# The keys() and values() methods will return a list of all keys/values in the dictionary

print(person1.keys())
print(person1.values())

# check the data type
print(type(names))
print(type(person1))

# The item() method will return each item in the dictionary, as key:value pair in a list
print(person1.items())

# Adding key:value pairs can be done by either putting a new index key and assigning a value or by using  the update() method
person1["nationality"] = "Filipino"
person1.update({"fave_food": "BBQ"})
print(person1)

# Deleting entries can be done using the pop() or the del keyword
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# Looping through dictionaries
for key in person1:
    print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries
person2 = {
    "name": "Monica",
    "age": 28,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

classroom = {
    "student1": person1,
    "student2": person2,
}

print(classroom)

# Functions
# Functions are blocks of code that can be run when called/invoked. A function can be used to get input, process the input, and return a output.

# The def keyword is used to create a function
# The function name is followed by a pair of parentheses

def my_greeting():
    # code to be executed when function is invoked
    print("Hello User!")

# Calling/invoking the function
my_greeting()

# Parameters can be added to functions to have more control as to what input the function will need
def greet_user(username):
    # prints out the value of the username parameters
    print(f"Hello {username}!")

greet_user("Bob")
greet_user("Amy")

# Return statements - the "return" keyword allows function to return values, just like in JS
def addition(num1, num2):
    return num1 + num2

sum = addition (5, 10 )
print(f"The sum of 5 and 10 is {sum}")

# A Lambda function is a small anonymous function that can be used for callbacks. It is just like any normal python function, excess that its name is defined as a variable, and usually contains just one line of code. A lambda function can take any number of arguments, but can only have one expression.

greeting = lambda name: f"Hello {name}!"
print(greeting("Anthony"))

mult = lambda a,b : a * b
print(mult(5, 6))

def increaser(num1):
    return lambda num2: num2 * num1

doubler = increaser(2)
print(doubler(11))
